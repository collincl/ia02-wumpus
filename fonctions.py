# GROUPE 12
# Penverne Léonard et Collin Clément

from random import randint

from wumpus import *
# ww = WumpusWorld(4, True)
ww = WumpusWorld()
n = ww.get_n()

from gopherpysat import Gophersat
gophersat_exec = "/Users/clementcollin/Desktop/UTC/GI02/IA02/gophersat"

voc = ["Wumpus", "Pit", "Safe"]
safe = [[False] * 4 for i in range(4)]
deduction = [["?"] * 4 for i in range(4)]
gs = [[Gophersat(gophersat_exec, voc), Gophersat(gophersat_exec, voc), Gophersat(gophersat_exec, voc), Gophersat(gophersat_exec, voc)] for i in range(4)]

def init_clauses() :
    for i in range(4) :
        for j in range(4) :
            gs[i][j].push_pretty_clause(["Wumpus", "Pit", "Safe"])
            gs[i][j].push_pretty_clause(["-Wumpus", "-Pit"])
            gs[i][j].push_pretty_clause(["-Pit", "-Safe"])
            gs[i][j].push_pretty_clause(["-Wumpus", "-Safe"])

def push_clauses(i, j, content):
    print("content = {}".format(content))
    if content == '.' :

        if i > 0 :
            gs[i - 1][j].push_pretty_clause(["Safe"])
        if i < 3 :
            gs[i + 1][j].push_pretty_clause(["Safe"])
        if j > 0 :
            gs[i][j - 1].push_pretty_clause(["Safe"])
        if j < 3 :
            gs[i][j + 1].push_pretty_clause(["Safe"])

    elif content == 'B' :

        if i > 0 :
            gs[i - 1][j].push_pretty_clause(["-Wumpus"])
        if i < 3 :
            gs[i + 1][j].push_pretty_clause(["-Wumpus"])
        if j > 0 :
            gs[i][j - 1].push_pretty_clause(["-Wumpus"])
        if j < 3 :
            gs[i][j + 1].push_pretty_clause(["-Wumpus"])

        if i == 0 :
            if j == 0 :
                if safe[0][1] :
                    gs[1][0].push_pretty_clause(["Pit"])
                elif safe[1][0] :
                    gs[0][1].push_pretty_clause(["Pit"])
            elif j == 3 :
                if safe[0][2] :
                    gs[1][3].push_pretty_clause(["Pit"])
                elif safe[1][3] :
                    gs[0][2].push_pretty_clause(["Pit"])
            else :
                if safe[i][j - 1] and safe[i][j + 1] :
                    gs[i + 1][j].push_pretty_clause(["Pit"])
                elif safe[i][j - 1] and safe[i + 1][j] :
                    gs[i][j + 1].push_pretty_clause(["Pit"])
                elif safe[i + 1][j] and safe[i][j + 1] :
                    gs[i][j - 1].push_pretty_clause(["Pit"])
        elif i == 3 :
            if j == 0 :
                if safe[2][0] :
                    gs[3][1].push_pretty_clause(["Pit"])
                elif safe[3][1] :
                    gs[2][0].push_pretty_clause(["Pit"])
            elif j == 3 :
                if safe[2][3] :
                    gs[3][2].push_pretty_clause(["Pit"])
                elif safe[3][2] :
                    gs[2][3].push_pretty_clause(["Pit"])
            else :
                if safe[i][j - 1] and safe[i][j + 1] :
                    gs[i - 1][j].push_pretty_clause(["Pit"])
                elif safe[i][j - 1] and safe[i - 1][j] :
                    gs[i][j + 1].push_pretty_clause(["Pit"])
                elif safe[i - 1][j] and safe[i][j + 1] :
                    gs[i][j - 1].push_pretty_clause(["Pit"])
        elif j == 0 :
            if safe[i - 1][j] and safe[i + 1][j] :
                gs[i][j + 1].push_pretty_clause(["Pit"])
            elif safe[i][j + 1] and safe[i - 1][j] :
                gs[i + 1][j].push_pretty_clause(["Pit"])
            elif safe[i + 1][j] and safe[i][j + 1] :
                gs[i - 1][j].push_pretty_clause(["Pit"])
        elif j == 3 :
            if safe[i - 1][j] and safe[i + 1][j] :
                gs[i][j - 1].push_pretty_clause(["Pit"])
            elif safe[i][j - 1] and safe[i - 1][j] :
                gs[i + 1][j].push_pretty_clause(["Pit"])
            elif safe[i + 1][j] and safe[i][j - 1] :
                gs[i - 1][j].push_pretty_clause(["Pit"])
        else :
            if safe[i][j - 1] and safe[i][j + 1] and safe[i - 1][j] :
                gs[i + 1][j].push_pretty_clause(["Pit"])
            elif safe[i][j - 1] and safe[i - 1][j] and safe[i + 1][j] :
                gs[i - 1][j].push_pretty_clause(["Pit"])
            elif safe[i][j - 1] and safe[i + 1][j] and safe[i - 1][j] :
                gs[i][j + 1].push_pretty_clause(["Pit"])
            elif safe[i][j + 1] and safe[i - 1][j] and safe[i + 1][j] :
                gs[i][j - 1].push_pretty_clause(["Pit"])

    elif content == 'S' :

        if i > 0 :
            gs[i - 1][j].push_pretty_clause(["-Pit"])
        if i < 3 :
            gs[i + 1][j].push_pretty_clause(["-Pit"])
        if j > 0 :
            gs[i][j - 1].push_pretty_clause(["-Pit"])
        if j < 3 :
            gs[i][j + 1].push_pretty_clause(["-Pit"])

        if i == 0 :
            if j == 0 :
                if safe[0][1] :
                    gs[1][0].push_pretty_clause(["Wumpus"])
                elif safe[1][0] :
                    gs[0][1].push_pretty_clause(["Wumpus"])
            elif j == 3 :
                if safe[0][2] :
                    gs[1][3].push_pretty_clause(["Wumpus"])
                elif safe[1][3] :
                    gs[0][2].push_pretty_clause(["Wumpus"])
            else :
                if safe[i][j - 1] and safe[i][j + 1] :
                    gs[i + 1][j].push_pretty_clause(["Wumpus"])
                elif safe[i][j - 1] and safe[i + 1][j] :
                    gs[i][j + 1].push_pretty_clause(["Wumpus"])
                elif safe[i + 1][j] and safe[i][j + 1] :
                    gs[i][j - 1].push_pretty_clause(["Wumpus"])
        elif i == 3 :
            if j == 0 :
                if safe[2][0] :
                    gs[3][1].push_pretty_clause(["Wumpus"])
                elif safe[3][1] :
                    gs[2][0].push_pretty_clause(["Wumpus"])
            elif j == 3 :
                if safe[2][3] :
                    gs[3][2].push_pretty_clause(["Wumpus"])
                elif safe[3][2] :
                    gs[2][3].push_pretty_clause(["Wumpus"])
            else :
                if safe[i][j - 1] and safe[i][j + 1] :
                    gs[i - 1][j].push_pretty_clause(["Wumpus"])
                elif safe[i][j - 1] and safe[i - 1][j] :
                    gs[i][j + 1].push_pretty_clause(["Wumpus"])
                elif safe[i - 1][j] and safe[i][j + 1] :
                    gs[i][j - 1].push_pretty_clause(["Wumpus"])
        elif j == 0 :
            if safe[i - 1][j] and safe[i + 1][j] :
                gs[i][j + 1].push_pretty_clause(["Wumpus"])
            elif safe[i][j + 1] and safe[i - 1][j] :
                gs[i + 1][j].push_pretty_clause(["Wumpus"])
            elif safe[i + 1][j] and safe[i][j + 1] :
                gs[i - 1][j].push_pretty_clause(["Wumpus"])
        elif j == 3 :
            if safe[i - 1][j] and safe[i + 1][j] :
                gs[i][j - 1].push_pretty_clause(["Wumpus"])
            elif safe[i][j - 1] and safe[i - 1][j] :
                gs[i + 1][j].push_pretty_clause(["Wumpus"])
            elif safe[i + 1][j] and safe[i][j - 1] :
                gs[i - 1][j].push_pretty_clause(["Wumpus"])
        else :
            if safe[i][j - 1] and safe[i][j + 1] and safe[i - 1][j] :
                gs[i + 1][j].push_pretty_clause(["Wumpus"])
            elif safe[i][j - 1] and safe[i - 1][j] and safe[i + 1][j] :
                gs[i - 1][j].push_pretty_clause(["Wumpus"])
            elif safe[i][j - 1] and safe[i + 1][j] and safe[i - 1][j] :
                gs[i][j + 1].push_pretty_clause(["Wumpus"])
            elif safe[i][j + 1] and safe[i - 1][j] and safe[i + 1][j] :
                gs[i][j - 1].push_pretty_clause(["Wumpus"])

def guess(content):
    if content == "W" :
        for m in range(4):
            for n in range(4):
                if not(ww.knowledge[m][n]) :
                    gs[m][n].push_pretty_clause(["-Wumpus"])
    for i in range(4):
        for j in range(4):
            if not(ww.knowledge[i][j]) :
                gs[i][j].push_pretty_clause(["-Safe"])
                A = gs[i][j].solve()
                gs[i][j].pop_clause()
                gs[i][j].push_pretty_clause(["-Wumpus"])
                B = gs[i][j].solve()
                # print(gs[i][j].get_pretty_model())
                gs[i][j].pop_clause()
                gs[i][j].push_pretty_clause(["-Pit"])
                C = gs[i][j].solve()
                # print(gs[i][j].get_pretty_model())
                gs[i][j].pop_clause()
                # print("A = {}, B = {}, C = {}".format(A, B, C))
                if not(A) and B and C :
                     safe[i][j] = True
                     deduction[i][j] = "Safe"
                     # print("deduction[{}][{}] = Safe".format(i, j))
                elif A and not(B) and C :
                     deduction[i][j] = "W"
                     ww.knowledge[i][j] = True
                     # si un seul Wumpus :
                     for m in range(4):
                         for n in range(4):
                             if not(ww.knowledge[m][n]) :
                                 gs[m][n].push_pretty_clause(["-Wumpus"])
                elif A and B and not(C) :
                     deduction[i][j] = "P"
                     ww.knowledge[i][j] = True

def isKnownWorld() :
    world_knowledge = True
    stop = False
    for m in range(4) :
        if stop :
            break
        for n in range(4) :
            if deduction[m][n] == "?" :
                world_knowledge = False
                stop = True
                break
    return world_knowledge
