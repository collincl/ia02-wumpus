# GROUPE 12
# Penverne Léonard et Collin Clément

from fonctions import *

init_clauses()
A, B, C = ww.probe(0, 0)
deduction[0][0] = B
push_clauses(0, 0, B)
guess(B)


while not(isKnownWorld()) :
    # for i in range(4) :
        # for j in range(4) :
            # print("deduction[{}][{}] = {}".format(i, j, deduction[i][j]))
    # import pdb; pdb.set_trace()
    stop = False
    for i in range(4) :
        if stop :
            break
        for j in range(4) :
            if not(ww.knowledge[i][j]) :
                if deduction[i][j] == "Safe" :
                    print("probe[{}][{}]".format(i, j))
                    A, B, C = ww.probe(i, j)
                    deduction[i][j] = B
                    push_clauses(i, j, B)
                    guess(B)
                    print(ww.get_knowledge())
                    stop = True
                    break
                elif i == 3 and j == 3 :
                    stop2 = False
                    for i in range(4) :
                        if stop2 :
                            break
                        for j in range(4) :
                            if deduction[i][j] == "?" :
                                x = i
                                y = j
                                stop2 = True
                                break
                    print("cautious_probe[{}][{}]".format(x, y))
                    A, B, C = ww.cautious_probe(x, y)
                    deduction[x][y] = B
                    push_clauses(x, y, B)
                    guess(B)
                    print(ww.get_knowledge())
                    stop = True
                    break

print("Coût exploration : {}".format(ww.get_cost()))
